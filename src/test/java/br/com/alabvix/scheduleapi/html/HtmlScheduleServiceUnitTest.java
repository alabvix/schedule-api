package br.com.alabvix.scheduleapi.html;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class HtmlScheduleServiceUnitTest {

    private HtmlScheduleService htmlScheduleService;

    @BeforeEach
    public void before() {
        htmlScheduleService = new HtmlScheduleService();
    }

    @Test
    public void generateHtmlSchedule_A4_MODEL1_3days_3fieldsPerPage() {
        StringBuffer htmlDocument;

        List<FieldData> days = new ArrayList<>();

        FieldData day1 = new FieldData("01", "Seg", "Jan", "2021");
        FieldData day2 = new FieldData("02", "Ter", "Jan", "2021");
        FieldData day3 = new FieldData("03", "Qua", "Jan", "2021");
        days.add(day1);
        days.add(day2);
        days.add(day3);

        HtmlScheduleInput input = new HtmlScheduleInput(PageEnum.A4, ModelEnum.MODEL_1, days, 3);
        htmlDocument = htmlScheduleService.generateHtmlSchedule(input);

        System.out.println(htmlDocument);

        assertNotNull(htmlDocument);

    }

}
