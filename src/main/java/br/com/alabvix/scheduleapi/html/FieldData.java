package br.com.alabvix.scheduleapi.html;

public class FieldData {

    public final String dayNumber;

    public final String dayName;

    public final String monthName;

    public final String year;

    public FieldData(String dayNumber, String dayName, String monthName, String year) {
        this.dayNumber = dayNumber;
        this.dayName = dayName;
        this.monthName = monthName;
        this.year = year;
    }
}
