package br.com.alabvix.scheduleapi.html;

import java.util.List;

public class HtmlScheduleInput {

    public final PageEnum page;

    public final ModelEnum model;

    public final List<FieldData> fields;

    public final int fieldsPerPage;

    public HtmlScheduleInput(PageEnum page, ModelEnum model, List<FieldData> fields, int fieldsPerPage) {
        this.page = page;
        this.model = model;
        this.fields = fields;
        this.fieldsPerPage = fieldsPerPage;
    }
}
