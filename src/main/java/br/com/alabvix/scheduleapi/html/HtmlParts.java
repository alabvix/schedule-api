package br.com.alabvix.scheduleapi.html;

public class HtmlParts {

    public final StringBuffer htmlDocument;

    public final StringBuffer htmlField;

    public HtmlParts(StringBuffer htmlDocument, StringBuffer htmlField) {
        this.htmlDocument = htmlDocument;
        this.htmlField = htmlField;
    }
}
