package br.com.alabvix.scheduleapi.html;

import br.com.alabvix.scheduleapi.schedule.ScheduleException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class HtmlScheduleService {

    private static final Logger LOGGER = Logger.getLogger(HtmlScheduleService.class.getName());

    /**
     * Generates the schedule html page. A page can have one or more page tags, and
     * a page tag can have one or more field tags.
     *
     * A field is the editable schedule component
     *
     * HTML FORMAT:
     *
     * <body>
     *     <page>
     *         <field></field>
     *         <field></field>
     *          <field></field>
     *     </page>
     *     <page>
     *        <field></field>
     *     </page>
     * </body>
     *
     * @param input
     * @return
     */
    public StringBuffer generateHtmlSchedule(HtmlScheduleInput input) {

        LOGGER.log(Level.INFO, "Starting generation of HTML schedule...");

        var htmlParts = loadHtmlParts(input.model, input.page);

        // Calculates the number os pages based on the number of days and paper size
        int totalPages = 0;
        if (input.fields.size() == 3) {
            totalPages = 1;
        }

        StringBuffer pages = new StringBuffer();
        for (int i=0; i<totalPages; i++) {

            pages.append(getStartPageTag(input.page));

            StringBuffer htmlField = new StringBuffer(htmlParts.htmlField);
            for (FieldData fieldData : input.fields) {
                pages.append(
                        parseFieldData(fieldData, htmlField)
                );
            }

            pages.append(getEndPageTag());
        }

        appendPages(htmlParts.htmlDocument, pages);

        LOGGER.log(Level.INFO, "Finished generation of HTML schedule.");

        return htmlParts.htmlDocument;
    }

    private void appendPages(StringBuffer htmlDocument, StringBuffer pages) {
        int start = htmlDocument.indexOf("$pages");
        htmlDocument.replace(start, start + 6, pages.toString());
    }

    private HtmlParts loadHtmlParts(ModelEnum model, PageEnum page) {

        StringBuffer htmlDocument = null;
        StringBuffer htmlField = null;

        if (model == ModelEnum.MODEL_1 && page == PageEnum.A4) {
            htmlDocument = loadHtmlFileAsString( "models/model_A4_1.html");
            htmlField = loadHtmlFileAsString( "models/model_A4_1_field.html");
        }

        return new HtmlParts(htmlDocument, htmlField);
    }

    private StringBuffer loadHtmlFileAsString(String resourcePathAndName) {

        File htmlFile = null;
        StringBuffer htmlString = null;

        try {
            htmlFile = new ClassPathResource(
                    resourcePathAndName).getFile();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Erro ao ler " + resourcePathAndName + ": " + e.getMessage());
            throw new ScheduleException("Erro ao ler modelo html");
        }

        try {
            htmlString = new StringBuffer(
                    String.valueOf(Files.readString(htmlFile.toPath()))
            );
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Erro ao converter " + resourcePathAndName + " para string: " + e.getMessage());
            throw new ScheduleException("Erro ao converter modelo html para string");
        }

        return htmlString;

    }

    private String getStartPageTag(PageEnum page) {
        return "<page size=\"" + page.name() + "\">\n";
    }

    private String getEndPageTag() {
        return "</page>\n";
    }

    private StringBuffer parseFieldData(FieldData fieldData, StringBuffer fieldElement) {

        String elementS = fieldElement.toString();

        elementS = elementS.replace("$dayNumber", fieldData.dayNumber);
        elementS = elementS.replace("$month", fieldData.monthName);
        elementS = elementS.replace("$dayName", fieldData.dayName);
        elementS = elementS.replace("$year", fieldData.year);

        StringBuffer fieldAdjusted = new StringBuffer(elementS);

        return fieldAdjusted;

    }

}
