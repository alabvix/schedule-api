package br.com.alabvix.scheduleapi.schedule;

public class ScheduleException extends RuntimeException{
    public ScheduleException(String message) {
        super(message);
    }
}
