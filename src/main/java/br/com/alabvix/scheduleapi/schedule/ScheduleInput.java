package br.com.alabvix.scheduleapi.schedule;

import br.com.alabvix.scheduleapi.html.ModelEnum;
import br.com.alabvix.scheduleapi.html.PageEnum;

import java.time.LocalDate;

public class ScheduleInput {

    public final PageEnum page;

    public final LocalDate startDate;

    public final LocalDate endDate;

    public final ModelEnum model;

    public final int fieldsPerPage;

    public ScheduleInput(PageEnum page, LocalDate startDate, LocalDate endDate, ModelEnum model, int fieldsPerPage) {
        this.page = page;
        this.model = model;
        this.startDate = startDate;
        this.endDate = endDate;
        this.fieldsPerPage = fieldsPerPage;
    }
}
