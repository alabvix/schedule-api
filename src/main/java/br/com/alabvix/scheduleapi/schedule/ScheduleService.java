package br.com.alabvix.scheduleapi.schedule;

import br.com.alabvix.scheduleapi.html.HtmlScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class ScheduleService {

    private static final Logger LOGGER = Logger.getLogger(ScheduleService.class.getName());

    private HtmlScheduleService htmlScheduleService;

    @Autowired
    public ScheduleService(HtmlScheduleService htmlScheduleService) {
        this.htmlScheduleService = htmlScheduleService;
    }

    public void generateSchedule(ScheduleInput input) {

        // TODO: Create the days...


        // TODO: Generate HTML schedule...


        // TODO: Convert to PDF..

    }


}

