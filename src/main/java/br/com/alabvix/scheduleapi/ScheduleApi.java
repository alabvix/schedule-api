package br.com.alabvix.scheduleapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScheduleApi {

	public static void main(String[] args) {
		SpringApplication.run(ScheduleApi.class, args);
	}

}
