# Schedule API

The purpose of this API is to generate Schedules and Planers for print.
There are model templates and configuration options:
- Paper size (A4 or A5)
- Fields per page
- timbre (dotted, no lines, lines, etc)

The schedule or planer is created as a PDF document.

Schedule example:
<p style="text-align: center;">
    <img src="example.PNG" alt="Example" >
</p>

